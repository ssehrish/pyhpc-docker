## 0.5.1 (release date: 2020-04-26)

* Update base image to heponhpc/hdfimg:0.5.1

## 0.5.0 (release date: 2020-04-26)

* Update to pandas 1.0.3

## 0.4.0 (release date: 2019-10-27)

* Replace use of pip by conda.

Removed installation of 'tables', because conda could not produce a
solution to the packaging matrix that included it.

Remove direct installation of numba, which is now in the underlying layer.

## 0.3.0 (release date: 2019-10-22)

* Update to newer pandas, and add numba.

## 0.2.0 (release date: 2018-11-05)

* Add tables 3.4.4, and dependencies.

## 0.1.0 (release date: 2018-11-04)

* Update to pandas 0.23.4

## 0.0.1 (release date: 2018-09-27)

* Initial release
