## 0.5.1 (release date: 2020-04-26)

* Update base to heponhpc/mpiimg:0.3.1

## 0.5.0 (release date: 2020-04-25)

* Update base to heponhpc/mpiimg:0.3.0
* Update to HDF5_VERSION = 1.10.6
* Update to H5PY_VERSION = 2.10.0

## 0.4.0 (release date: 2019-10-25)

* Replace use of pip for installation with use of conda for installation.
  Note that this can result in updates to all parts of the conda installation
  (even the python version, or the version of conda itself).

## 0.3.0 (release date: 2018-11-04)

* Move to heponhpc/hdf5img
* Update HDF5 to version 1.10.4
* Update cython to 0.29.0
* Update h5py to 2.8.0, and get it from GitHub rather than PythonHosted


## 0.0.1 (release date: 2018-05-01)

* Initial release
