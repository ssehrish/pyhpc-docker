## 0.6.1 (release date: 2020-04-26)

* do 'conda clean' after installation

## 0.6.0 (release date: 2020-04-24)

* Updated to Python 3.7.7 and numpy 1.18.1

## 0.1.1 (release date: 2018-11-04)

* Changed name to `heponhpc/pyimg`.

## 0.1.0 (release date: 2018-05-03)

* Removed pandas 

## 0.0.1 (release date: 2018-05-01)

* Initial release
