FROM heponhpc/pyimg:0.6.1
ARG VERSION
ARG MPI4PY_VERSION
ARG MPICH_VERSION
LABEL VERS=$VERSION

# Manual installation of mpich seems to be required to work on NERSC                        
# options for configure are the same used by the mpich ups package                          
RUN mkdir -p /build-mpich \
    && mkdir -p /mpich-install \
    && cd /build-mpich \
    && wget  http://www.mpich.org/static/downloads/${MPICH_VERSION}/mpich-$MPICH_VERSION.tar.gz \
    && tar -xvzf mpich-${MPICH_VERSION}.tar.gz \
    &&  cd mpich-${MPICH_VERSION} \
    && CFLAGS="-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64" ./configure --prefix=/mpich-install --enable-fortran=all --disable-cxx --enable-romio --enable-debuginfo --enable-threads=runtime --disable-wrapper-rpath --enable-shared --disable-static --disable-libxm --enable-timer-type=clock_gettime --enable-g=debug --enable-mpit-pvars=none --enable-strict --enable-fast=ndebug,O3 \
    && make -j 4 \
    && make install \
    && rm -rf /build-mpich                                                                  
    
# mpi4py
RUN mkdir /build-mpi4py && cd /build-mpi4py \
    && wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-${MPI4PY_VERSION}.tar.gz \
    && tar -zxvf mpi4py-${MPI4PY_VERSION}.tar.gz && cd mpi4py-${MPI4PY_VERSION} \
    && python setup.py build --mpicc=/mpich-install/bin/mpicc \
    && python setup.py install \
    && rm -rf /build-mpi4py
   
ENV X_PATH=/mpich-install/bin:$PATH
ENV PATH=$X_PATH
ENV LD_LIBRARY_PATH=/mpich-install/lib

COPY mpiimg_login.sh /etc/profile.d/mpiimg_login.sh 
