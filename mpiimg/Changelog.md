## 0.3.1 (release date: 2020-04-26)

* Update to use of pyimg 0.6.1

## 0.3.0 (release date: 2020-04-24)

* Update to use of pyimg 0.6.0
* Update to MPICH 3.3.2 and mpi4py 3.0.3

## 0.2.4 (release date: 2018-11-04)

* Changed name to heponhpc/mpiimg, now based on heponhpc/pyimg:0.1.1

## 0.0.1 (release date: 2018-05-01)

* Initial release
